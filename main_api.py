import json
from flask import Flask, request, jsonify
from flasgger import Swagger
from flask_cors import *
import traceback

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
app.config['JSONIFY_MIMETYPE'] = "application/json;charset=utf-8"
CORS(app, suppors_credentials=True, resources={r'/*'})  # 设置跨域
swagger = Swagger(app)

import formula_mgr
from formula.formula_base import MyMLError
from config import Config

@app.route('/api/p/medical/formula', methods=['POST'])
def medical_formula():
    try:
        body = request.get_json()
        formula_name, params = body['formula_name'], body['params']
        result = formula_mgr.infer(formula_name, params)
        return json.dumps({'result': result, 'result_code': 0}, ensure_ascii=False)
    except MyMLError as e:
        print(e)
        traceback.print_exc()
    except Exception as e:
        print(e)
        traceback.print_exc()
        return json.dumps({'resultMsg': '异常,请联系开发者', 'result_code': 1}, ensure_ascii=False)


if __name__ == '__main__':
    app.run('0.0.0.0', port=Config.PYTHON_PORT)
