#!/usr/bin/python
# encoding: utf-8
class colorectalCancer():
    """
    结直肠癌评估
    http://www.doc88.com/p-0886432605027.html
    https://wenku.baidu.com/view/20f9ff92964bcf84b8d57b3b.html
    """

    def __init__(self):
        """
        评估模型包含症状与体征、家族疾病史、疾病史、生活习惯六种因素
        """
        self.signs_and_symptoms = {"慢性便秘": 2.01, "慢性腹泻": 3.82, "大便潜血": 10.13}
        self.family_history = {"一级家属肠癌史": 2.42}
        self.disease_history = {"消化道溃疡史": 1.91, "胆囊疾病或胆囊手术史": 2.01, "慢性阑尾炎或阑尾手术史": 2.09, "慢性结直肠炎病史": 4.14, "肠息肉史": 6.86,
                                "黏液便史": 9.51}
        self.habits_and_customs = {"吃蔬菜>=1次/d": 0.78, "吸烟": 1.09, "饮酒": 1.11}
        self.age_and_sex = {"25": {"男性": 14.25, "女性": 10.35}, "30": {"男性": 23.65, "女性": 17.70},
                            "35": {"男性": 40.69, "女性": 30.10}, "40": {"男性": 70.28, "女性": 49.89},
                            "45": {"男性": 122.57, "女性": 77.82}, "50": {"男性": 203.14, "女性": 134.06},
                            "55": {"男性": 317.55, "女性": 192.17}, "60": {"男性": 499.55, "女性": 287.69},
                            "65": {"男性": 712.40, "女性": 394.17}, "70": {"男性": 910.24, "女性": 517.91},
                            "75": {"男性": 1164.43, "女性": 646.50}, "80": {"男性": 1157.96, "女性": 665.93}}

    def calc(self, data):
        weights_one = 0
        for children in data['symptom']:
            if children in self.signs_and_symptoms:
                weights_one = weights_one + self.signs_and_symptoms[children] - 1
        for children in data['FDH']:
            if children in self.family_history:
                weights_one = weights_one + self.family_history[children] - 1
        for children in data['PDH']:
            if children in self.disease_history:
                weights_one = weights_one + self.disease_history[children] - 1
        for children in data['HAC']:
            if children in self.habits_and_customs:
                if children == "吃蔬菜>=1次/d":
                    weights_one = weights_one + self.habits_and_customs[children]
                else:
                    weights_one = weights_one + self.habits_and_customs[children] - 1
        if 'smoke' in data:
            if int(data['smoke']) > 2:
                weights_one = weights_one + self.habits_and_customs["吸烟"] - 1
        if 'alcohol' in data:
            if int(data['alcohol']) > 2:
                weights_one = weights_one + self.habits_and_customs["饮酒"] - 1
        age = int(data['age'])
        if age >= 25:
            age_weights = []
            age_status = 0
            for key, value in self.age_and_sex.items():
                if age >= int(key):
                    age_weights.append(value)
                    age_status = age_status + 1
                if age_status > 2:
                    break
            age_weights = age_weights[len(age_weights) - 1]
            if data['gender'] == "1":
                age_weights = age_weights['男性'] / 1000000
            else:
                age_weights = age_weights['女性'] / 1000000
            return {"结直肠癌评估结果": round(age_weights * weights_one * 100, 7), "危险因素较同龄人群倍数": round(weights_one, 3)}
        else:
            return {"结直肠癌评估结果": "无需检查"}


if __name__ == '__main__':
    print(colorectalCancer().calc(
        {"gender": "1", "age": "63", "FDH": ['无'], "PDH": ['无'], "symptom": ['无'],
         "HAC": ["吃蔬菜>=1次/d"], "smoke": 1, "alcohol": 4, }))
