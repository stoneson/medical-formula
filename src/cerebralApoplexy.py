#!/usr/bin/python
# encoding: utf-8
class cerebralApoplexy():
    """
    脑卒中评估
    http://wjw.beijing.gov.cn/bmfw_20143/jkzs/jbzs/201912/t20191217_1250368.html
    """
    def __init__(self):
        """
        评估模型包含吸烟、喝酒、服用药物、家族疾病史、疾病史、运动、bmi、治疗史、收缩压、舒张压10种因素
        """
        self.sport = {'每天': 1, '每周一次以上': 2, '偶尔': 3, '不锻炼': 4}
        self.family_history = ["脑卒中"]
        self.disease_history = ["高血压", "房颤和心瓣膜病", "糖尿病", "血脂异常", "高胱氨酸血症", "劲动脉狭窄", "脑卒中", "心房颤动", "短暂性脑缺血"]
        self.drug = ["避孕药", "降压药"]
        self.treatment = ["调脂治疗", '降压治疗']
        self.smoking = {"从不吸烟": 1, "已戒烟": 2, "吸烟": 3}
        self.drinking = {"从不": 1, "偶尔": 2, "经常": 3, "每天": 4}
        "------------------------------------------------------------"
        self.high_risk_disease = ["短暂性脑缺血", "脑卒中"]
        self.middle_risk_disease = ["高血压", "房颤和心瓣膜病", "糖尿病", "心房颤动"]

    def calc(self, data):
        bmi = pow(float(data['height']) / float(data['weight']), 2)
        status = 0
        my_age = int(data['age'])
        if bmi >= 26:
            status = status + 1
        if my_age >= 40:
            status = status + 1
        if 'smoke' in data:
            if int(data['smoke']) > 2:
                status = status + 1
        if 'alcohol' in data:
            if int(data['alcohol']) > 2:
                status = status + 1
        if 'FDH' in data and my_age >= 35:
            for element in data['FDH']:
                if element in self.family_history:
                    status = status + 1
        if 'PDH' in data:
            for element in data['PDH']:
                if element in self.disease_history:
                    status = status + 1
        if 'sport' in data:
            if int(data['sport']) > 2:
                status = status + 1
        if 'SBP' in data:
            if int(data['SBP']) >= 140:
                status = status + 1
        if 'DBP' in data:
            if int(data['DBP']) >= 90:
                status = status + 1
        if 'TH' in data:
            for element in data['TH']:
                if element in self.treatment:
                    status = status + 1
        if 'MH' in data:
            for element in data['MH']:
                if element in self.drug:
                    status = status + 1
        if status >= 3 or list(set(data['PDH']).intersection(set(self.high_risk_disease))) != []:
            return {"脑卒中评估结果": "脑卒中高危人群"}
        elif status < 3 and list(set(data['PDH']).intersection(set(self.middle_risk_disease))) != []:
            return {"脑卒中评估结果": "脑卒中中危人群"}
        else:
            return {"脑卒中评估结果": "脑卒中低危人群"}
