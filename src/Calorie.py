#!/usr/bin/python
# encoding: utf-8
class calorie():
    """
    计算卡路里
    """

    def __init__(self):
        self.type = {"久坐": 1.2, "轻运动": 1.375, "中等运动": 1.55, "剧烈运动": 1.725, "运动员": 1.9}  # 运动类型
        self.user_type = {"0": "久坐", "1": "轻运动", "2": "中等运动", "3": "剧烈运动", "4": "运动员"}

    def calculation_bmi(self, age, weight, zhi_fang, height):
        """
        计算每日所需最低卡路里及bmi
        """
        bmi = weight / pow(height / 100, 2)
        bmr = int(10 * weight + 6.25 * height - 5 * age - 1620 * zhi_fang + 377.5)
        tdee = {}
        for key, value in self.type.items():
            tdee[key] = int(bmr * value)
        tdee['基础代谢率'] = bmr
        if bmi >= 30:
            bmi_type = '肥胖'
        elif 25 <= bmi < 30:
            bmi_type = '超重'
        elif 18.5 <= bmi < 25:
            bmi_type = '正常重量'
        else:
            bmi_type = '体重过轻'
        return bmr, tdee, int(bmi), bmi_type

    def dram_weight(self, sex, height):
        """
        计算理想体重
        """
        height = height * 3.28084 / 100
        if sex == '1':
            data1 = round(int(height / 5) * 52 + (height % 5) * 12 * 1.9)
            data2 = round(int(height / 5) * 56.2 + (height % 5) * 12 * 1.41)
            data3 = round(int(height / 5) * 48 + (height % 5) * 12 * 2.7)
            data4 = round(50 + (height * 12 - 60) * 2.3)
        else:
            data1 = round(int(height / 5) * 52 + (height % 5) * 12 * 1.7)
            data2 = round(int(height / 5) * 56.2 + (height % 5) * 12 * 1.36)
            data3 = round(int(height / 5) * 48 + (height % 5) * 12 * 2.2)
            data4 = round(45.5 + (height * 12 - 60) * 2.3)
        return max(data1, data2, data3, data4), min(data1, data2, data3, data4)

    def nutrition(self, tdee):
        """
        计算摄入能量
        """
        return {"营养建议": {"中等碳水化合物": {"蛋白质": int(tdee * 0.4 / 4), "脂肪": int(tdee * 0.4 / 9),
                                     "碳水化合物": int(tdee * 0.2 / 4)},
                         "低等碳水化合物": {"蛋白质": int(tdee * 0.3 / 4), "脂肪": int(tdee * 0.35 / 9),
                                     "碳水化合物": int(tdee * 0.35 / 4)},
                         "高等碳水化合物": {"蛋白质": int(tdee * 0.3 / 4), "脂肪": int(tdee * 0.2 / 9),
                                     "碳水化合物": int(tdee * 0.5 / 4)}}}

    def calc(self, data):
        """
        主函数data = {"gender": "1", "sport_type": '0', "age": "35", "weight": "55", "height": "170", "body_fat": "参数可选"}
        """
        sex = data['gender']
        sport_type = self.user_type[data['ST']]
        age = int(data['age'])
        weight = float(data['weight'])
        height = float(data['height'])
        if data['BF'] != '':
            zhi_fang = int(data['BF']) / 100
        elif data['gender'] == '1':
            zhi_fang = 0.23
        else:
            zhi_fang = 0.3324
        bmr, tdee, bmi, bmi_type = self.calculation_bmi(age, weight, zhi_fang, height)
        max_dram, min_dram = self.dram_weight(sex, height)
        tdee_user = tdee[sport_type]
        result = {}
        result.update(self.nutrition(tdee_user))
        result['理想体重'] = {"max": int(max_dram), "min": int(min_dram)}
        result['BMI'] = bmi
        result['BMI_type'] = bmi_type
        result['TDEE'] = tdee
        result['用户TDEE'] = tdee_user
        return result


if __name__ == '__main__':
    data = {"gender": "1", "ST": '0', "age": "35", "weight": "55", "height": "170", "BF": ""}
    print(calorie().calc(data))
