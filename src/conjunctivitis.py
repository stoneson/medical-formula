#!/usr/bin/python
# encoding: utf-8
class conjunctivitis():
    """
    结膜炎评估
    """

    def __init__(self):
        """
        评估模型包含吸烟、喝酒、职业接触史、传染病接触史、疾病史、生活习惯7种因素
        """
        self.contact_history = ["结膜炎"]
        self.disease_history = ["衣原体感染", "干眼症", "睑缘炎", "肺结核", "梅毒", "肺结核", "梅毒", "甲状腺疾病", "淋球菌感染", "移植物抗宿主病", "川崎病"
            , "结膜色素痣", "乳头状瘤", "皮样瘤", "血管瘤", "囊肿", "结膜鳞状细胞癌", "恶性黑色素瘤", "泪管堵塞","结膜炎"]
        self.smoking = {"从不吸烟": 1, "已戒烟": 2, "吸烟": 3}
        self.drinking = {"从不": 1, "偶尔": 2, "经常": 3, "每天": 4}
        self.habits_and_customs = ["佩戴隐形眼镜", "水质不洁地方游泳", "手和袖子擦眼睛", "熬夜", "压力过大"]
        self.occupation = ["毛发", "花粉", "皮屑", "化学物品", "风沙", "烟尘"]

    def calc(self, data):
        status = 0
        if 'smoke' in data:
            if int(data['smoke']) > 2:
                status = status + 1
        if 'alcohol' in data:
            if int(data['alcohol']) > 3:
                status = status + 1
        if 'CHID' in data:
            for element in data['CHID']:
                if element in self.contact_history:
                    status = status + 1
        if 'PDH' in data:
            for element in data['PDH']:
                if element in self.disease_history:
                    status = status + 1
        if 'HAC' in data:
            for element in data['HAC']:
                if element in self.habits_and_customs:
                    status = status + 1
        if 'OCH' in data:
            for element in data['OCH']:
                if element in self.occupation:
                    status = status + 1
        if status >= 1:
            return {"结膜炎评估结果": "结膜炎高危人群"}
        else:
            return {"结膜炎评估结果": "结膜炎低危人群"}
