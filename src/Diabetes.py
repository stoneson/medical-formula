class Diabetes:
    """
    糖尿病检测
    """

    def __init__(self):
        self.sport = {'每天': 1, '每周一次以上': 2, '偶尔': 3, '不锻炼': 4}

    def calc(self, data):
        """
        参数：{"weight"：int，"height"：int，"age"：int，"gender"：1/0，"waist"：float，"FDH"：[疾病]，"PDH"：[疾病],"sport"：1每天、2每周一次、3偶尔、4不锻炼,"SBP":int,"DBP":int,"TH":[治疗],"MH":[药物]}
        """
        bmi = pow(float(data['height']) / float(data['weight']), 2)
        status = 0
        my_age = int(data['age'])
        if my_age >= 18:
            if bmi >= 24:
                status = status + 1
            if 'waist' in data:
                if float(data['waist']) >= 90 and data['gender'] == '1':
                    status = status + 1
                if float(data['waist']) >= 85 and data['gender'] == '0':
                    status = status + 1
            if my_age >= 40:
                status = status + 1
            if 'FDH' in data and my_age >= 35:
                for element in data['FDH']:
                    if element.find('糖') != -1 or element == 'ASCVD':
                        status = status + 1
            if 'PDH' in data:
                for element in data['PDH']:
                    if element.find('糖') != -1 or element == 'ASCVD':
                        status = status + 1
            if 'sport' in data:
                if int(data['sport']) > 2:
                    status = status + 1
            if 'SBP' in data:
                if int(data['SBP']) >= 140:
                    status = status + 1
            if 'DBP' in data:
                if int(data['DBP']) >= 90:
                    status = status + 1
            if 'TH' in data:
                for element in data['TH']:
                    if element == "调脂治疗" or element == '降压治疗':
                        status = status + 1
            if 'MH' in data:
                for element in data['MH']:
                    if element.find('精神药物') != -1 or element.find('抑郁药物') != -1:
                        status = status + 1
        if status != 0:
            return {"糖尿病评估结果": "疑似糖尿病"}
        else:
            return {"糖尿病评估结果": "无法确定糖尿病"}


