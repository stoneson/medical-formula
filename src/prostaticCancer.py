#!/usr/bin/python
# encoding: utf-8
class prostaticCancer():
    """
    前列腺癌评估
    https://www.baidu.com/bh/dict/ydxx_7791455887852768698?tab=%E6%A6%82%E8%BF%B0&title=%E5%89%8D%E5%88%97%E8%85%BA%E7%99%8C&contentid=ydxx_7791455887852768698&query=%E5%89%8D%E5%88%97%E8%85%BA%E7%99%8C&sf_ref=dict_home&from=dicta
    https://wenku.baidu.com/view/d8dbdfe8a0c7aa00b52acfc789eb172ded63999d.html
    """

    def __init__(self):
        """
        评估模型包含吸烟、喝酒、职业接触史、家族疾病史、疾病史、生活习惯、bmi、症状8种因素
        """
        self.family_history = ["前列腺癌", "非息肉性结直肠癌", "乳腺癌"]
        self.disease_history = ["糖尿病", "高血压", "前列腺疾病", "性传播疾病"]
        self.habits_and_customs = ["大量的肉类及（或）乳制品"]
        self.signs_and_symptoms = ["性早熟"]
        self.smoking = {"从不吸烟": 1, "已戒烟": 2, "吸烟": 3}
        self.drinking = {"从不": 1, "偶尔": 2, "经常": 3, "每天": 4}
        self.occupation = ["镉", "橡胶"]

    def calc(self, data):
        if data['gender'] == 1:
            bmi = pow(float(data['height']) / float(data['weight']), 2)
            status = 0
            if bmi > 30:
                status = status + 1
            if 'OCH' in data:
                for element in data['OCH']:
                    if element in self.occupation:
                        status = status + 1
            if 'symptom' in data:
                for element in data['symptom']:
                    if element in self.signs_and_symptoms:
                        status = status + 1
            if 'FDH' in data:
                for element in data['FDH']:
                    if element in self.family_history:
                        status = status + 1
            if 'HAC' in data:
                for element in data['HAC']:
                    if element in self.habits_and_customs:
                        status = status + 1
            if 'PDH' in data:
                for element in data['PDH']:
                    if element in self.disease_history:
                        status = status + 1
            if 'alcohol' in data:
                if int(data['alcohol']) > 3:
                    status = status + 1
            if 'smoke' in data:
                if int(data['smoke']) >= 3:
                    status = status + 1
            if 'PSA' in data:
                if float(data['PSA']) >= 1:
                    status = status + 1
            if data['age'] >= 50:
                status = status + 1
            if status >= 1:
                return {"前列腺癌评估结果": "前列腺癌高危人群"}
            else:
                return {"前列腺癌评估结果": "前列腺癌低危人群"}
        else:
            return {"前列腺癌评估结果": "无需前列腺癌评估"}
