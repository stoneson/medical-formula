#!/usr/bin/python
# encoding: utf-8
class gastricCancer():
    """
    胃癌评估
    https://m.baidu.com/bh/m/detail/qr_12273910869026659812
    https://wenku.baidu.com/view/337fa8bb3386bceb19e8b8f67c1cfad6185fe976.html
    """

    def __init__(self):
        """
        评估模型包含吸烟、喝酒、居住环境、家族疾病史、疾病史、运动、生活习惯6种因素
        """
        self.family_history = ["胃癌"]
        self.disease_history = ["慢性萎缩性胃炎", "胃溃疡", "胃息肉", "手术后残胃", "肥厚性胃炎", "恶性贫血","幽门螺杆菌感染"]
        self.smoking = {"从不吸烟": 1, "已戒烟": 2, "吸烟": 3}
        self.drinking = {"从不": 1, "偶尔": 2, "经常": 3, "每天": 4}
        self.habits_and_customs = ["摄入高盐", "喜欢吃腌制饮食"]
        self.environment = ["东北地区", "华北地区", "福建", "江苏", "上海", "西北地区"]
        "------------------------------------------------------------"
        self.high_risk_disease = ["幽门螺杆菌感染"]

    def calc(self, data):
        status = 0
        my_age = int(data['age'])
        if my_age >= 40:
            status = status + 1
        if 'smoke' in data:
            if int(data['smoke']) > 2:
                status = status + 1
        if 'alcohol' in data:
            if int(data['alcohol']) > 2:
                status = status + 1
        if 'FDH' in data and my_age >= 35:
            for element in data['FDH']:
                if element in self.family_history:
                    status = status + 1
        if 'PDH' in data:
            for element in data['PDH']:
                if element in self.disease_history:
                    status = status + 1
        if 'HAC' in data:
            for element in data['HAC']:
                if element in self.habits_and_customs:
                    status = status + 1
        if 'environment' in data:
            for element in data['environment']:
                if element in self.environment:
                    status = status + 1
        if status >1:
            return {"胃癌评估结果": "符合胃癌筛查人群"}
        else:
            return {"胃癌评估结果": "暂不符合胃癌筛查人群"}
