#!/usr/bin/python
# encoding: utf-8
class esophagealCancer():
    """
    食管癌评估
    """

    def __init__(self):
        """
        评估模型包含吸烟、喝酒、居住环境、家族疾病史、疾病史、bmi、生活习惯7种因素
        """
        self.family_history = ["食管癌"]
        self.disease_history = ["头颈部癌症", "呼吸道鳞癌", "胃食管反流病", "人乳头瘤病毒感染"]
        self.smoking = {"从不吸烟": 1, "已戒烟": 2, "吸烟": 3}
        self.drinking = {"从不": 1, "偶尔": 2, "经常": 3, "每天": 4}
        self.habits_and_customs = ["喜食烫食", "喜欢吃腌制饮食"]
        self.environment = ["河北", "河南", "川北", "福建", "广东", "新疆", "苏北"]
        self.symptoms = ["恶心", "呕吐", "腹痛", "反酸", "进食不适"]

    def calc(self, data):
        status = 0
        my_age = int(data['age'])
        if my_age >= 40:
            if pow(float(data['height']) / float(data['weight']), 2) >= 26:
                status = status + 1
            if 'smoke' in data:
                if int(data['smoke']) > 2:
                    status = status + 1
            if 'alcohol' in data:
                if int(data['alcohol']) > 3:
                    status = status + 1
            if 'FDH' in data:
                for element in data['FDH']:
                    if element in self.family_history:
                        status = status + 1
            if 'PDH' in data:
                for element in data['PDH']:
                    if element in self.disease_history:
                        status = status + 1
            if 'HAC' in data:
                for element in data['HAC']:
                    if element in self.habits_and_customs:
                        status = status + 1
            if 'environment' in data:
                for element in data['environment']:
                    if element in self.environment:
                        status = status + 1
            if status >= 1:
                return {"食管癌评估结果": "食管癌高危人群"}
            else:
                return {"食管癌评估结果": "暂不符合食管癌筛查人群"}
        else:
            return {"食管癌评估结果": "暂不符合食管癌筛查人群"}
