#!/usr/bin/python
# encoding: utf-8
class cervicalCarcinoma():
    """
    宫颈癌评估
    https://www.sohu.com/a/305634370_100129888
    https://www.baidu.com/bh/dict/ydxx_8197253237021734135?tab=%E6%A6%82%E8%BF%B0&title=%E5%AE%AB%E9%A2%88%E7%99%8C&contentid=ydxx_8197253237021734135&query=%E5%AE%AB%E9%A2%88%E7%99%8C&sf_ref=dict_home&from=dicta
    """

    def __init__(self):
        """
        评估模型包含吸烟、家族疾病史、疾病史、生活习惯、症状5种因素
        """
        self.habits_and_customs = ["多个性伴侣", "初次性生活＜16岁", "初产年龄小", "多孕多产", "卫生条件差", "营养不良"]
        self.family_history = ["宫颈癌"]
        self.disease_history = ["沙眼衣原体感染", "单纯疱疹病毒II型感染", "滴虫感染", "HPV感染"]
        self.symptom = ["生殖道出血", "宫颈糜烂"]
        self.smoking = {"从不吸烟": 1, "已戒烟": 2, "吸烟": 3}

    def calc(self, data):
        if int(data['gender']) == 0:
            status = 0
            my_age = int(data['age'])
            if 'smoke' in data:
                if int(data['smoke']) > 2:
                    status = status + 1
            if 'FDH' in data and my_age >= 35:
                for element in data['FDH']:
                    if element in self.family_history:
                        status = status + 1
            if 'PDH' in data:
                for element in data['PDH']:
                    if element in self.disease_history:
                        status = status + 1
            if 'symptom' in data:
                for element in data['symptom']:
                    if element in self.symptom:
                        status = status + 1
            if status >= 1:
                return {"宫颈癌评估结果": "宫颈癌高危人群"}
            else:
                return {"宫颈癌评估结果": "宫颈癌低危人群"}
        else:
            return {"宫颈癌评估结果": "无需宫颈癌检查"}
