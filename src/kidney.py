#!/usr/bin/python
# encoding: utf-8
class kidney():
    """
    慢性肾脏病
    """

    def __init__(self):
        self.dis = ['糖尿病', '高尿酸血症', '高血压', '痛风']

    def disease_handle(self, data):
        status = 0
        data_age = int(data['age'])
        if data_age >= 65:
            status = status + 1
        if data_age >= 35:
            for element in data['FDH']:
                if element.find('肾') != -1 or element in self.dis:
                    status = status + 1
        for element in data['PDH']:
            if element.find('肾') != -1 or element in self.dis:
                status = status + 1
        if status != 0:
            return "疑似慢性肾脏病高危人群"
        else:
            return "无法确定慢性肾脏病"

    def calc(self, data):
        data_age = int(data['age'])
        # if data['血肌酐单位'] == 'mg/dL':
        #     Cockcroft_Gault = (140 - int(data['年龄'])) * int(data['体重']) / (72 * 88.4 * float(data['血肌酐']))
        # else:
        #     Cockcroft_Gault = (140 - int(data['年龄'])) * int(data['体重']) / (72 * float(data['血肌酐']))
        if 'SC' in data:
            Cockcroft_Gault = (140 - data_age) * int(data['height']) / (72 * float(data['SC']))
            if data['gender'] == '0':
                Cockcroft_Gault = 0.85 * Cockcroft_Gault
            if Cockcroft_Gault < 15:
                return {"慢性肾脏病评估结果": "疑似慢性肾脏病"}
            else:
                return {"慢性肾脏病评估结果": self.disease_handle(data)}
        else:
            return {"慢性肾脏病评估结果": self.disease_handle(data)}
