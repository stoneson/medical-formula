#!/usr/bin/python
# encoding: utf-8
class lungCancer():
    """
    肺癌评估
    https://wenku.baidu.com/view/e01a08dfad51f01dc281f186.html
    """

    def __init__(self):
        """
        评估模型包含吸烟史、职业史、居住环境、家族疾病史、疾病史、食物六种因素
        """
        self.history_of_tobacco = {"被动吸烟": 1.3, "已戒烟": {"戒烟年数>10": 1.0, "戒烟年数<10": 2.0},
                                   "吸烟": {"0": 1.8, "100": 2.6, "200": 4.2, "300": 5.8, "400": 8,
                                          "烟斗或旱烟": 4.6}}
        self.occupation = {"石棉": 9.0, "硅粉尘": 2.6, "煤烟或焦油": 2.2}
        self.living_city = {"大城市(空气污染）": 1.3}
        self.family_history = {"肺癌": 1.6}
        self.disease_history = {"肺结核": 2.6, "慢性支气管炎": 2.4, "肺炎": 2.0}
        self.intake_of_vegetables_and_fruits = {"水果蔬菜摄入量<400g/d": 1.4}
        self.risk_factor_percentage = {"已戒烟": 0.014, "吸烟指数<100": 0.07, "吸烟指数<199": 0.11, "吸烟指数<299": 0.14,
                                       "吸烟指数<399": 0.16, "吸烟指数>400": 0.12, "烟斗或旱烟": 0.05, "石棉": 0.006,
                                       "硅粉尘": 0.024, "煤烟或焦油": 0.024, "大城市(空气污染）": 0.14, "肺癌家族史": 0.12, "肺结核疾病史": 0.04,
                                       "慢性支气管炎疾病史": 0.04, "肺炎疾病史": 0.06, "蔬菜水果摄入": 0.56}
        self.risk_factor_weight = {"已戒烟": 2.0, "吸烟指数<100": 1.8, "吸烟指数<199": 2.6, "吸烟指数<299": 4.2,
                                   "吸烟指数<399": 5.8, "吸烟指数>400": 8, "烟斗或旱烟": 4.6, "石棉": 9.0,
                                   "硅粉尘": 2.6, "煤烟或焦油": 2.2, "大城市(空气污染）": 1.3, "肺癌家族史": 1.6, "肺结核疾病史": 2.6,
                                   "慢性支气管炎疾病史": 2.4, "肺炎疾病史": 2.0, "蔬菜水果摄入": 1.4}

    def index_smoking(self, data):
        smoking_index = 100 * int(int(data['SLC']['年史']) * int(data['SLC']['每天吸烟数']) / 100)
        if smoking_index > 400:
            smoking_index = 400
        return self.history_of_tobacco['吸烟'][str(smoking_index)]

    def calc(self, data):
        index = 1
        if data['SLC']['吸烟史'] != '无':
            if data['SLC']['吸烟史'] == "吸烟" or data['SLC']['吸烟史'] == "已戒烟":
                if data['SLC']['烟型'] == "纸烟":
                    index = self.index_smoking(data)
                else:
                    index = self.history_of_tobacco[data['SLC']['烟型']]
                if data['SLC']['吸烟史'] == "已戒烟":
                    if int(data['SLC']['戒烟年数']) >= 10:
                        index = index * self.history_of_tobacco['已戒烟']["戒烟年数>10"]
                    else:
                        index = index * self.history_of_tobacco['已戒烟']["戒烟年数<10"]
            else:
                index = self.history_of_tobacco['被动吸烟']
        if data['OCH']:
            for element_ in data['OCH']:
                if element_ != '无接触史':
                    index = index * self.occupation[element_]
        if data['environment']:
            for children in data['environment']:
                if children != '无':
                    index = index * self.living_city[children]
        if data['FDH']:
            for element in data['FDH']:
                if element != '无':
                    index = index * self.family_history[element]
        if data['PDH']:
            for element_one in data['PDH']:
                if element_one != '无':
                    index = index * self.disease_history[element_one]
        if data['HAC']:
            for element_one in data['HAC']:
                if element_one == '水果蔬菜摄入量<400g/d':
                    index = index * self.intake_of_vegetables_and_fruits[element_one]
        index_weight_all = 1
        for key, value in self.risk_factor_weight.items():
            temp1 = round(float(value) * self.risk_factor_percentage[key], 2)
            temp2 = round(1 - self.risk_factor_percentage[key], 2)
            index_weight_all = index_weight_all * (temp1 + temp2)
        index_weight = round(index / index_weight_all, 2)
        if data['SLC']['吸烟史'] == "已戒烟":
            index_weight = index_weight * 2 / 5.8
        if index_weight <= 0.5:
            result = "显著低于一般人群"
        elif 0.5 < index_weight <= 0.9:
            result = "低于一般人群"
        elif 0.5 < index_weight <= 0.9:
            result = "相当于一般人群"
        elif 0.9 < index_weight <= 1.1:
            result = "高于一般人群"
        else:
            result = "显著高于一般人群"
        return {"肺癌评估结果": result}


if __name__ == '__main__':
    print(lungCancer().calc({"HAC": ["水果蔬菜摄入量<400g/d"], "PDH": [], "FDH": [], "environment": ['大城市(空气污染）'], "OCH": [],
                             "SLC": {"吸烟史": "已戒烟", "年史": "20", "烟型": "纸烟", "每天吸烟数": "46", "戒烟年数": "2"}}))
