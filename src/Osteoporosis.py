#!/usr/bin/python
# encoding: utf-8
class Osteoporosis():
    """
    骨质疏松症评估
    https://wenku.baidu.com/view/e78bf4edf8b069dc5022aaea998fcc22bcd143ad.html
    https://mp.weixin.qq.com/s/fio_MX2UuHmJ5vB1EVZRNw
    """

    def __init__(self):
        """
        评估模型包含吸烟、喝酒、服用药物、家族疾病史、疾病史、生活习惯、bmi、治疗史、症状9种因素
        """
        self.family_history = ["父母年龄<75岁时骨折"]
        self.disease_history = ["糖尿病", "库欣综合征", "甲状腺功能亢进症", "甲状旁腺功能亢进症", "性激素缺乏/性腺功能减退症", "肢端肥大症", "类风湿关节炎",
                                "慢性器官衰竭（肾、肝、心、肺）", "肠炎"]
        self.smoking = {"从不吸烟": 1, "已戒烟": 2, "吸烟": 3}
        self.drinking = {"从不": 1, "偶尔": 2, "经常": 3, "每天": 4}
        self.signs_and_symptoms = ["阳痿", "缺乏性欲", "腹泻", "初潮晚", "绝经早（<45岁）", "绝经前期雌激素分泌不足（无月经连续12个月）", "轻微碰撞导致骨折"]
        self.drug = ["抗癫痫药", "糖皮质激素", "肝素", "噻唑烷二酮", "可的松", "泼尼松"]
        self.habits_and_customs = ["大量摄取咖啡（每天超过4杯）", "大量摄取浓茶", "大量摄取碳酸饮料", "身高比年轻时降低（超过3cm)", "缺乏维生素", "缺乏钙"]

    def calc(self, data):
        index = (int(data['weight']) - int(data['age'])) * 0.2
        bmi = pow(float(data['height']) / float(data['weight']), 2)
        status = 0
        if bmi <= 18:
            status = status + 1
        if 'MH' in data:
            for element in data['MH']:
                if element in self.drug:
                    status = status + 1
        if 'symptom' in data:
            for element in data['symptom']:
                if element in self.signs_and_symptoms:
                    status = status + 1
        if 'FDH' in data:
            for element in data['FDH']:
                if element in self.family_history:
                    status = status + 1
        if 'HAC' in data:
            for element in data['HAC']:
                if element in self.habits_and_customs:
                    status = status + 1
        if 'PDH' in data:
            for element in data['PDH']:
                if element in self.disease_history:
                    status = status + 1
        if 'sport' in data:
            if int(data['sport']) > 2:
                status = status + 1
        if 'alcohol' in data:
            if int(data['alcohol']) > 3:
                status = status + 1
        if 'smoke' in data:
            if int(data['smoke']) >= 3:
                status = status + 1
        if status >= 1 or index <= -4:
            return {"骨质疏松症评估结果": "骨质疏松症高危人群"}
        elif -1 <= index < -4:
            return {"骨质疏松症评估结果": "骨质疏松症中危人群"}
        else:
            return {"骨质疏松症评估结果": "骨质疏松症低危人群"}
