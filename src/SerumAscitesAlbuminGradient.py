#!/usr/bin/python
# encoding: utf-8
class SerumAscitesAlbuminGradient():
    """
    http://doctor-network.com/public/littletools/261.html
    """

    def __init__(self):
        """
       SAAG （Serum ascites albumin gradient，血清腹水白蛋白梯度）
        """

    def calc(self, data):
        # 血清白蛋白 Serum albumin g/L
        # 腹水白蛋白 Ascites albumin
        SA = float(data['SA'])
        AA = float(data['AA'])
        SAAG = round(SA - AA, 2)
        if SAAG >= 11:
            status = "可能患有门脉高压"
        else:
            status = "不存在门脉高压"
        return {"SAAG": SAAG, "status": status}
