#!/usr/bin/python
# encoding: utf-8
class mammaryCancer():
    """
    乳腺癌评估
    https://www.baidu.com/bh/dict/ydxx_7926947288468097761?tab=%E6%A6%82%E8%BF%B0&title=%E4%B9%B3%E8%85%BA%E7%99%8C&contentid=ydxx_7926947288468097761&query=%E4%B9%B3%E8%85%BA%E7%99%8C&sf_ref=dict_home&from=dicta
    https://m.haodf.com/touch/zhuanjiaguandian/wugaosong_7961529486.htm
    """

    def __init__(self):
        """
        评估模型包含喝酒、治疗、家族疾病史、疾病史、生活习惯、bmi、治疗史、症状8种因素
        """
        self.disease_history = ["乳腺疾病", "乳腺导管", "小叶中重度不典型增生", "小叶原位癌"]
        self.treatment = ["胸部放疗", "停经后进行雌激素替代疗法"]
        self.first_family_history = ["卵巢上皮", "输卵管癌", "原发性腹膜癌"]
        self.first_family_history_one = ["原发性乳腺癌"]
        self.symptoms = ["月经初潮年龄早（<12岁）", "绝经年龄晚（＞55岁）", "不孕及初次生育年龄晚（＞30岁）", "哺乳时间短"]
        self.family_history = ["乳腺癌", "遗传性乳腺及卵巢综合征", "多发性错构瘤综合征", "利-弗劳梅尼综合征", "波伊茨-耶格综合征", "林奇综合征"]
        self.habits_and_customs = ['高脂饮食']

    def calc(self, data):
        if int(data['gender']) == 0:
            status = 0
            age = data['age']
            bmi = pow(float(data['height']) / float(data['weight']), 2)
            if bmi > 26:
                status = status + 1
            if "MCFH" in data:
                status_one = 0
                status_two = 0
                for element in data['first_family']:
                    if "age" in element:
                        for element_one in element['first_family_history']:
                            if element_one in self.first_family_history:
                                status_two = status_two + 1
                            if element_one == self.first_family_history_one:
                                status_one = status_one + 1
                        if int(element['age']) <= 45:
                            status = status + 1
                        elif 45 <= int(element['age']) <= 50 and status_two > 0:
                            status = status + 1
                        if int(element['age']) <= 50 and status_one >= 2:
                            status = status + 1
                        if data['gender'] == 1:
                            status = status + 1
                    if status_two >= 2:
                        status = status + 1
            if 'TH' in data:
                for element in data['TH']:
                    if element in self.treatment:
                        status = status + 1
            if 'DH' in data:
                for element in data['DH']:
                    if element in self.disease_history:
                        status = status + 1
            if 'symptom' in data:
                for element in data['symptom']:
                    if element in self.symptoms:
                        status = status + 1
            if 'FDH' in data:
                for element in data['FDH']:
                    if element in self.family_history + self.first_family_history_one + self.first_family_history:
                        status = status + 1
            if 'HAC' in data:
                for element in data['HAC']:
                    if element in self.habits_and_customs:
                        status = status + 1
            if status > 0:
                return {"乳腺癌评估结果": "专家强烈推荐乳腺检查", "乳腺癌检查方法": ["乳腺X线筛查"]}
            elif 45 <= age <= 69:
                return {"乳腺癌评估结果": "专家强烈推荐乳腺检查", "乳腺癌检查方法": ["乳腺X线筛查"]}
            elif 40 <= age <= 44:
                return {"乳腺癌评估结果": "专家中等推荐乳腺检查", "乳腺癌检查方法": ["乳腺超声筛查"]}
            elif 40 < age:
                return {"乳腺癌评估结果": "专家一般推荐乳腺检查", "乳腺癌检查方法": ["乳腺磁共振筛查"]}
            else:
                return {"乳腺癌评估结果": "专家中等推荐乳腺检查", "乳腺癌检查方法": ["乳腺超声筛查"]}
        else:
            return {"乳腺癌评估结果": "暂无需乳腺检查"}
