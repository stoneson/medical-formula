#!/usr/bin/python
# encoding: utf-8
class liverCancer():
    """
    肝癌评估
    https://m.baidu.com/bh/m/detail/qr_12273910869026659812
    https://wenku.baidu.com/view/0369ac460029bd64793e2c9a.html#
    """

    def __init__(self):
        """
        评估模型包含喝酒、居住环境、家族疾病史、疾病史4种因素
        """
        self.family_history = ["肝癌"]
        self.disease_history = ["慢性乙型肝炎病毒感染", "慢性丙型肝炎病毒感染", "血吸虫肝硬化", "酒精性肝硬化", "原发性胆汁性肝硬化", "药物性肝损伤", "血色病",
                                "α-1抗胰蛋白酶缺乏症", "糖原贮积病", "迟发性皮肤卟啉症", "酪氨酸血症", "自身免疫性肝炎", "非酒精性脂肪肝"]
        self.drinking = {"从不": 1, "偶尔": 2, "经常": 3, "每天": 4}
        self.environment = ["东南地区", "东北地区", "沿海岛屿", "江河海口"]
        "------------------------------------------------------------"

    def calc(self, data):
        status = 0
        my_age = int(data['age'])
        if 'alcohol' in data:
            if int(data['alcohol']) > 2:
                status = status + 1
        if 'FDH' in data and my_age >= 35:
            for element in data['FDH']:
                if element in self.family_history:
                    status = status + 1
        if 'PDH' in data:
            for element in data['PDH']:
                if element in self.disease_history:
                    status = status + 1
        if 'environment' in data:
            for element in data['environment']:
                if element in self.environment:
                    status = status + 1
        if my_age >= 45 and data['gender'] == 0:
            if status >= 1:
                return {"肝癌评估结果": "肝癌高危人群"}
            else:
                return {"肝癌评估结果": "肝癌低危人群"}
        elif my_age >= 35 and data['gender'] == 1:
            if status >= 1:
                return {"肝癌评估结果": "肝癌高危人群"}
            else:
                return {"肝癌评估结果": "肝癌低危人群"}
        else:
            return {"肝癌评估结果": "暂无肝癌风险人群"}
