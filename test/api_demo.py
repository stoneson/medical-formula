# -*- coding: utf-8 -*-
# @Time    : 2020/6/15 10:22
# @Author  : nijian
import json
import requests


def to_calorie():
    """
    卡路里测试
    :return:
    """
    data = {"formula_name": "人体所需卡路里",
            "params": {"性别": "男", "运动类别": '0', "年龄": "35", "体重": "55", "身高": "165", "体脂": ""}}
    res = requests.post(url="http://127.0.0.1:6001/api/p/medical/formula", json=data)
    resp = res.json()
    print(json.dumps(resp, ensure_ascii=False, indent=2))


def to_xuexing():
    """    血型测试
    :return:
    """
    data = {"formula_name": "人体血型",
            "params": {"父亲": "A", "母亲": "A"}}
    res = requests.post(url="http://127.0.0.1:6001/api/p/medical/formula", json=data)
    resp = res.json()
    print(json.dumps(resp, ensure_ascii=False, indent=2))


if __name__ == "__main__":
    to_calorie()
